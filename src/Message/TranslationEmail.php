<?php

// src/Message/TranslationEmail.php

declare(strict_types=1);

namespace App\Message;

use App\Model\TranslationRequest;

/**
 * Class TranslationEmail.
 */
class TranslationEmail
{
    /**
     * @var string
     */
    private $locale;

    /**
     * @var TranslationRequest
     */
    private $translationRequest;

    /**
     * TranslationEmail constructor.
     *
     * @param TranslationRequest $translationRequest
     * @param string             $locale
     */
    public function __construct(TranslationRequest $translationRequest, string $locale = '')
    {
        $this->translationRequest = $translationRequest;
        $this->locale = $locale;
    }

    /**
     * @return TranslationRequest
     */
    public function getTranslationRequest(): TranslationRequest
    {
        return $this->translationRequest;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale ? $this->locale : 'en';
    }
}
