<?php

// src/Service/AppTranslationService.php

declare(strict_types=1);

namespace App\Service;

use App\Exception\CacheException;
use App\Exception\TranslateException;
use Panda\Yandex\TranslateSDK\Cloud;
use Panda\Yandex\TranslateSDK\Translate;
use Panda\Yandex\TranslateSDK\Format;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AppTranslationService.
 */
class AppTranslationService
{
    /**
     * Cache kay for languages list.
     */
    const LANG_LIST_CACHE_KEY = 'YANDEX_TRANSLATE_LANGUAGES_LIST';

    /**
     * Cache ttl for languages list.
     */
    const LANG_LIST_CACHE_TTL = 86400;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var Cloud
     */
    private $cloud;

    /**
     * @var AppCacheService
     */
    private $appCacheService;

    /**
     * AppTranslationService constructor.
     *
     * @param AppCacheService     $appCacheService
     * @param TranslatorInterface $translator
     * @param Cloud               $cloud
     */
    public function __construct(AppCacheService $appCacheService, TranslatorInterface $translator, Cloud $cloud)
    {
        $this->translator = $translator;
        $this->cloud = $cloud;
        $this->appCacheService = $appCacheService;
    }

    /**
     * @param string $text
     * @param string $destinationLanguage
     *
     * @return \stdClass
     *
     * @throws CacheException
     * @throws TranslateException
     */
    public function translate(string $text, string $destinationLanguage): \stdClass
    {
        if (!$text) {
            throw new TranslateException('Text cannot be empty');
        }

        if (!in_array($destinationLanguage, $this->getLanguagesList(), true)) {
            throw new TranslateException('Destination language not exist');
        }

        // prepare translation request
        $translate = new Translate($text);
        $translate->setTargetLang($destinationLanguage);
        $translate->setFormat(Format::PLAIN_TEXT);

        // translate text in yandex cloud
        $responseString = $this->cloud->request($translate);

        // parse response
        return json_decode($responseString);
    }

    /**
     * @return array
     *
     * @throws CacheException
     */
    public function getLanguagesList()
    {
        try {
            $languagesList = $this->appCacheService->get(self::LANG_LIST_CACHE_KEY);
        } catch (CacheException $cacheException) {
            $languagesList = null;
        }

        if (!$languagesList) {
            $languagesList = $this->cloud->getLanguageList();
            $this->appCacheService->set(self::LANG_LIST_CACHE_KEY, $languagesList, self::LANG_LIST_CACHE_TTL);
        }
        $languagesList = json_decode($languagesList);
        $languages = property_exists($languagesList, 'languages') ? $languagesList->languages : [];
        $result = [];
        foreach ($languages as $language) {
            if (false === property_exists($language, 'name')) {
                continue;
            }
            if (property_exists($language, 'code')) {
                $result[$language->name] = $language->code;
            }
        }

        return $result;
    }
}
