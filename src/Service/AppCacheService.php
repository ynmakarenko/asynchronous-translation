<?php

// src/Service/AppCacheService.php

declare(strict_types=1);

namespace App\Service;

use App\Exception\CacheException;
use Predis\Response\Status;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AppCacheService.
 */
class AppCacheService
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var \Predis\Client
     */
    private $redisClient;

    /**
     * AppCacheService constructor.
     *
     * @param TranslatorInterface $translator
     * @param \Predis\Client      $redisClient
     */
    public function __construct(TranslatorInterface $translator, \Predis\Client $redisClient)
    {
        $this->translator = $translator;
        $this->redisClient = $redisClient;
    }

    /**
     * @param string $key
     *
     * @return string
     *
     * @throws \Exception
     */
    public function get(string $key): string
    {
        if (!$this->redisClient->exists($key)) {
            throw new CacheException("Key '$key' not exist in cache.");
        }

        return $this->redisClient->get($key);
    }

    /**
     * @param string $key
     * @param string $value
     * @param int    $ttlSeconds
     *
     * @return bool
     *
     * @throws CacheException
     */
    public function set(string $key, string $value, int $ttlSeconds = 0): bool
    {
        if ($ttlSeconds < 0) {
            throw new CacheException('Cache TTL can not be less zero.');
        }

        /** @var Status $result */
        $result = null;
        if ($ttlSeconds > 0) {
            $result = $this->redisClient->set($key, $value, 'EX', $ttlSeconds);
        } else {
            $result = $this->redisClient->set($key, $value);
        }

        return 'OK' === $result->getPayload();
    }
}
