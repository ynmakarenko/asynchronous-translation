<?php

// src/Service/AppEmailService.php

declare(strict_types=1);

namespace App\Service;

use App\Exception\EmailException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class AppEmailService
{
    /**
     * Default from email property.
     */
    const DEFAULT_FROM_EMAIL = 'no-reply@affility.work';

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * AppEmailService constructor.
     *
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $html
     *
     * @return bool
     *
     * @throws EmailException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendEmail(string $from, string $to, string $subject, string $html): bool
    {
        // if "from" empty
        if (!$from) {
            // apply default "from" value
            $from = self::DEFAULT_FROM_EMAIL;
        }

        if (!$to) {
            throw new EmailException('"To" must be set for send email');
        }
        if (!$subject) {
            throw new EmailException('"Subject" must be set for send email');
        }
        if (!$html) {
            throw new EmailException('"Html" must be set for send email');
        }

        // send email
        $this->mailer->send((new Email())
            ->from($from)
            ->to($to)
            ->subject($subject)
            ->html($html));

        return true;
    }
}
