<?php

// src/Form/TranslationRequestType.php

declare(strict_types=1);

namespace App\Form;

use App\Model\TranslationRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class TranslationRequestType.
 */
class TranslationRequestType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * TranslationRequestType constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // form option languages_choices with destination languages data must be set in controller
        if (false === isset($options['languages_choices'])
            || false === is_array($options['languages_choices'])
            || 0 === count($options['languages_choices'])) {
            throw new \Exception($this->translator->trans('exceptions.empty_languages_choices'));
        }
        $languagesChoices = $options['languages_choices'];

        $builder
            ->add('text', TextareaType::class, [
                'label' => 'translation.page.form.text.label',
                'help' => 'translation.page.form.text.help',
                'attr' => [
                    'rows' => 3,
                    'maxlength' => 512,
                    'placeholder' => 'translation.page.form.text.placeholder',
                ],
            ])
            ->add('destLang', ChoiceType::class, [
                'choices' => $languagesChoices,
                'label' => 'translation.page.form.dest_lang.label',
                'help' => 'translation.page.form.dest_lang.help',
                'attr' => [
                    'placeholder' => 'translation.page.form.dest_lang.placeholder',
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'translation.page.form.email.label',
                'help' => 'translation.page.form.email.help',
                'attr' => [
                    'placeholder' => 'translation.page.form.email.placeholder',
                ],
            ])
            ->add('translate', SubmitType::class, [
                'label' => 'translation.page.form.submit.label',
                'attr' => [
                    'class' => 'btn-primary',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TranslationRequest::class,
            'languages_choices' => null,
        ]);
    }
}
