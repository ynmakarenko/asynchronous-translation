<?php

// src/Controller/TranslationController.php

declare(strict_types=1);

namespace App\Controller;

use App\Form\TranslationRequestType;
use App\Message\TranslationEmail;
use App\Model\TranslationRequest;
use App\Service\AppTranslationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class TranslationController.
 */
class TranslationController extends AbstractController
{
    /**
     * @Route("/", name="translator_index")
     *
     * @param AppTranslationService $appTranslationService
     * @param Request               $request
     * @param MessageBusInterface   $bus
     * @param TranslatorInterface   $translator
     *
     * @return RedirectResponse|Response
     *
     * @throws \App\Exception\CacheException
     */
    public function index(AppTranslationService $appTranslationService, Request $request, MessageBusInterface $bus, TranslatorInterface $translator)
    {
        $translationRequest = new TranslationRequest();

        // Set translation to English language by default
        $translationRequest->setDestLang(!empty($_ENV['DEFAULT_DESTINATION_LANGUAGE_CODE']) ? $_ENV['DEFAULT_DESTINATION_LANGUAGE_CODE'] : 'en');

        $form = $this->createForm(TranslationRequestType::class, $translationRequest, [
           'languages_choices' => $appTranslationService->getLanguagesList(),
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // dispatch new event to bus
            $bus->dispatch(new TranslationEmail($translationRequest, $request->getLocale()));

            // add success message for user
            $this->addFlash(
                'success',
                $translator->trans('translation.sent.success').' '.$translationRequest->getEmail()
            );

            // redirect to current page for reset form values
            return $this->redirect($this->generateUrl('translator_index'));
        }

        // render translation form
        return $this->render('translator/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
