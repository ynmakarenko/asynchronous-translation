<?php

// src/Exception/TranslateException.php

declare(strict_types=1);

namespace App\Exception;

class TranslateException extends \Exception
{
}
