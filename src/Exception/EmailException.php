<?php

// src/Exception/EmailException.php

declare(strict_types=1);

namespace App\Exception;

class EmailException extends \Exception
{
}
