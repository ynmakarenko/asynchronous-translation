<?php

// src/Exception/CacheException.php

declare(strict_types=1);

namespace App\Exception;

class CacheException extends \Exception
{
}
