<?php

// src/Validator/DestLang.php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DestLang extends Constraint
{
    /**
     * @var string
     */
    public $message = 'The destination language "{{ value }}" is not valid.';
}
