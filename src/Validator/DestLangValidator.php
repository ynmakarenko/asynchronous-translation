<?php

// src/Validator/DestLangValidator.php

declare(strict_types=1);

namespace App\Validator;

use App\Service\AppTranslationService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * Class DestLangValidator.
 */
class DestLangValidator extends ConstraintValidator
{
    /**
     * @var AppTranslationService
     */
    private $appTranslationService;

    /**
     * DestLangValidator constructor.
     *
     * @param AppTranslationService $appTranslationService
     */
    public function __construct(AppTranslationService $appTranslationService)
    {
        $this->appTranslationService = $appTranslationService;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     *
     * @throws \App\Exception\CacheException
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof DestLang) {
            throw new UnexpectedTypeException($constraint, DestLang::class);
        }

        /* @var $constraint \App\Validator\DestLang */

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        $langList = $this->appTranslationService->getLanguagesList();

        if (!in_array($value, $langList, true)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }
    }
}
