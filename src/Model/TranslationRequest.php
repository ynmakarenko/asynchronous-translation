<?php

// src/Model/TranslationRequest.php

declare(strict_types=1);

namespace App\Model;

use App\Validator as AppAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TranslationRequest.
 */
class TranslationRequest
{
    /**
     * @Assert\NotBlank(message = "validator.not_blank")
     * @Assert\Type(
     *     type = "string",
     *     message = "validator.type"
     * )
     * @Assert\Length(
     *      max = 512,
     *      maxMessage = "validator.length_max"
     * )
     *
     * @var string
     */
    private $text;

    /**
     * @Assert\NotBlank(message = "validator.not_blank")
     * @Assert\Type(
     *     type = "string",
     *     message = "validator.type"
     * )
     * @AppAssert\DestLang
     *
     * @var string
     */
    private $destLang;

    /**
     * @Assert\NotBlank(message = "validator.not_blank")
     * @Assert\Type(
     *     type = "string",
     *     message = "validator.type"
     * )
     * @Assert\Email(
     *     message = "validator.email"
     * )
     *
     * @var string
     */
    private $email;

    public function __construct(string $text = '', string $destLang = '', string $email = '')
    {
        $this->text = $text;
        $this->destLang = $destLang;
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getDestLang(): string
    {
        return $this->destLang;
    }

    /**
     * @param string $destLang
     */
    public function setDestLang(string $destLang): void
    {
        $this->destLang = $destLang;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}
