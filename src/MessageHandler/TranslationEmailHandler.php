<?php

// src/MessageHandler/TranslationEmailHandler.php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Exception\CacheException;
use App\Exception\EmailException;
use App\Exception\TranslateException;
use App\Message\TranslationEmail;
use App\Service\AppEmailService;
use App\Service\AppTranslationService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Class TranslationEmailHandler.
 */
class TranslationEmailHandler implements MessageHandlerInterface
{
    const RESULT_EMAIL_SUBJECT = 'Translation completed';

    /**
     * @var AppTranslationService
     */
    private $appTranslationService;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var AppEmailService
     */
    private $appEmailService;

    /**
     * TranslationEmailHandler constructor.
     *
     * @param AppTranslationService $appTranslationService
     * @param Environment           $twig
     * @param TranslatorInterface   $translator
     * @param AppEmailService       $appEmailService
     */
    public function __construct(AppTranslationService $appTranslationService, Environment $twig, TranslatorInterface $translator, AppEmailService $appEmailService)
    {
        $this->appTranslationService = $appTranslationService;
        $this->twig = $twig;
        $this->translator = $translator;
        $this->appEmailService = $appEmailService;
    }

    /**
     * @param TranslationEmail $message
     *
     * @throws CacheException
     * @throws EmailException
     * @throws TranslateException
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function __invoke(TranslationEmail $message): void
    {
        // retrieve translation request
        $translationRequest = $message->getTranslationRequest();

        // retrieve and ser user locale
        if (method_exists($this->translator, 'setLocale')) {
            $this->translator->setLocale($message->getLocale());
        }

        // translate text to destination language
        $translationResult = $this->appTranslationService->translate($translationRequest->getText(), $translationRequest->getDestLang());

        // send email with results to user email
        $this->appEmailService->sendEmail(
            $_ENV['MAILGUN_FROM_EMAIL'],
            $translationRequest->getEmail(),
            self::RESULT_EMAIL_SUBJECT,
            $this->twig->render('translator/email.html.twig', [
                'translationRequest' => $translationRequest,
                'translationResult' => $translationResult,
            ])
        );
    }
}
