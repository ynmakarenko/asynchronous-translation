# Available commands

* `composer test` - run all tests: phpunit, phpstan and phpcs
* `composer test:phpunit` - run unit tests
* `test:phpstan` - run static analysis tool
* `test:phpcs` - run code sniffer checker
* `fix:phpcs` - run code sniffer fixer

