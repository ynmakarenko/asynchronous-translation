<?php

namespace App\Tests\MessageHandler;

use App\Message\TranslationEmail;
use App\MessageHandler\TranslationEmailHandler;
use App\Model\TranslationRequest;
use App\Service\AppEmailService;
use App\Service\AppTranslationService;
use PHPUnit\Framework\MockObject\Stub;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class TranslationEmailHandlerTest extends TestCase
{
    /**
     * @var AppTranslationService|Stub
     */
    private $appTranslationService;

    /**
     * @var Stub|Environment
     */
    private $twig;

    /**
     * @var Stub|TranslatorInterface
     */
    private $translator;

    /**
     * @var AppEmailService|Stub
     */
    private $appEmailService;

    public function setUp(): void
    {
        parent::setUp();
        $this->appTranslationService = $this->createStub(AppTranslationService::class);
        $this->twig = $this->createStub(Environment::class);
        $this->translator = $this->createStub(TranslatorInterface::class);
        $this->appEmailService = $this->createStub(AppEmailService::class);
    }

    public function testShould_SendEmail_When_TranslationIsCompleted(): void
    {
        /**
         * Arrange.
         */
        $userLocale = 'en';

        /*
         * Assert
         */
        $this->appTranslationService->expects($this->once())->method('translate');
        $this->appEmailService->expects($this->once())->method('sendEmail');

        /**
         * Act.
         */
        $translationEmailHandler = new TranslationEmailHandler($this->appTranslationService, $this->twig, $this->translator, $this->appEmailService);
        $translationEmail = new TranslationEmail(new TranslationRequest('Hello', 'ru', 'mail@mail.com'), $userLocale);
        $translationEmailHandler->__invoke($translationEmail);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->appTranslationService = null;
        $this->twig = null;
        $this->translator = null;
        $this->appEmailService = null;
    }
}
