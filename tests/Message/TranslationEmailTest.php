<?php

// tests/Message/TranslationEmailTest.php

declare(strict_types=1);

namespace App\Tests\Message;

use App\Message\TranslationEmail;
use App\Model\TranslationRequest;
use PHPUnit\Framework\TestCase;

class TranslationEmailTest extends TestCase
{
    public function testShould_UseDefaultEnLocale_When_LocaleNotPassed(): void
    {
        /**
         * Act.
         */
        $translationEmail1 = new TranslationEmail(new TranslationRequest());
        $translationEmail2 = new TranslationEmail(new TranslationRequest(), '');
        $translationEmail3 = new TranslationEmail(new TranslationRequest(), 'ru');

        /*
         * Assert
         */
        $this->assertEquals('en', $translationEmail1->getLocale());
        $this->assertEquals('en', $translationEmail2->getLocale());
        $this->assertEquals('ru', $translationEmail3->getLocale());
    }
}
