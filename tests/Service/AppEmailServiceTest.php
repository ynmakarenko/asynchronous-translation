<?php

// tests/Service/AppEmailServiceTest.php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Exception\EmailException;
use App\Service\AppEmailService;
use PHPUnit\Framework\MockObject\Stub;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;

/**
 * Class AppEmailServiceTest.
 */
class AppEmailServiceTest extends TestCase
{
    /**
     * @var Stub|MailerInterface
     */
    private $mailer;

    public function setUp(): void
    {
        parent::setUp();
        $this->mailer = $this->createStub(MailerInterface::class);
    }

    public function testShould_SendEmail(): void
    {
        /**
         * Arrange.
         */
        $from = AppEmailService::DEFAULT_FROM_EMAIL;
        $to = 'ynmakarenko@gmail.com';
        $subject = 'Test';
        $html = '<h1>Test</h1>';

        /**
         * Act.
         */
        $appEmailService = new AppEmailService($this->mailer);
        $result = $appEmailService->sendEmail($from, $to, $subject, $html);

        /*
         * Assert
         */
        $this->assertTrue($result);
    }

    public function testShould_ThrowException_When_ToIsEmpty(): void
    {
        /**
         * Arrange.
         */
        $from = AppEmailService::DEFAULT_FROM_EMAIL;
        $to = '';
        $subject = 'Test';
        $html = '<h1>Test</h1>';

        /*
         * Assert
         */
        $this->expectException(EmailException::class);

        /**
         * Act.
         */
        $appEmailService = new AppEmailService($this->mailer);
        $appEmailService->sendEmail($from, $to, $subject, $html);
    }

    public function testShould_ThrowException_When_SubjectIsEmpty(): void
    {
        /**
         * Arrange.
         */
        $from = AppEmailService::DEFAULT_FROM_EMAIL;
        $to = 'ynmakarenko@gmail.com';
        $subject = '';
        $html = '<h1>Test</h1>';

        /*
         * Assert
         */
        $this->expectException(EmailException::class);

        /**
         * Act.
         */
        $appEmailService = new AppEmailService($this->mailer);
        $appEmailService->sendEmail($from, $to, $subject, $html);
    }

    public function testShould_ThrowException_When_HtmlIsEmpty(): void
    {
        /**
         * Arrange.
         */
        $from = AppEmailService::DEFAULT_FROM_EMAIL;
        $to = 'ynmakarenko@gmail.com';
        $subject = 'Test';
        $html = '';

        /*
         * Assert
         */
        $this->expectException(EmailException::class);

        /**
         * Act.
         */
        $appEmailService = new AppEmailService($this->mailer);
        $appEmailService->sendEmail($from, $to, $subject, $html);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->mailer = null;
    }
}
