<?php

// tests/Service/AppCacheServiceTest.php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Exception\CacheException;
use App\Service\AppCacheService;
use PHPUnit\Framework\MockObject\Stub;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AppCacheServiceTest.
 */
class AppCacheServiceTest extends TestCase
{
    const EMPTY_REDIS_CONNECTION_EXCEPTION_MESSAGE = 'Redis connection string must be set in .env file';

    /**
     * @var Stub|TranslatorInterface
     */
    private $translator;

    /**
     * @var Stub|\Predis\Client
     */
    private $redisClient;

    public function setUp(): void
    {
        parent::setUp();
        $this->translator = $this->createStub(TranslatorInterface::class);
        $this->translator
            ->method('trans')
            ->willReturn(self::EMPTY_REDIS_CONNECTION_EXCEPTION_MESSAGE);

        $this->redisClient = $this->getMockBuilder(\Predis\Client::class)
            ->addMethods(['exists', 'set', 'get'])
            ->getMock();
    }

    public function testShould_ThrowException_When_CacheKeyNotExist(): void
    {
        /**
         * Arrange.
         */
        $randCacheKey = 'random111';

        $this->redisClient->method('exists')
            ->willReturn(false);
        $this->redisClient->expects($this->once())
            ->method('exists');
        $this->redisClient->expects($this->never())
            ->method('get');

        /*
         * Assert
         */
        $this->expectException(CacheException::class);
        $this->expectExceptionMessage("Key '$randCacheKey' not exist in cache.");

        /**
         * Act.
         */
        $appCacheService = new AppCacheService($this->translator, $this->redisClient);
        $appCacheService->get($randCacheKey);
    }

    public function testShould_ThrowException_When_SetCacheTtlIsNegative(): void
    {
        /**
         * Arrange.
         */
        $testCacheKey = 'test_key';
        $testCacheValue = 'test_value';

        /*
         * Assert
         */
        $this->expectException(CacheException::class);
        $this->expectExceptionMessage('Cache TTL can not be less zero.');

        /**
         * Act.
         */
        $appCacheService = new AppCacheService($this->translator, $this->redisClient);
        $appCacheService->set($testCacheKey, $testCacheValue, -1);
    }

    public function testShould_ReturnTrue_When_CacheSavedWithPositiveTtl(): void
    {
        /**
         * Arrange.
         */
        $testCacheKey = 'test_key';
        $testCacheValue = 'test_value';
        $testCacheTtl = 1;

        $resultValue = new class() {
            public function getPayload()
            {
                return 'OK';
            }
        };
        $this->redisClient->method('set')
            ->willReturn($resultValue);
        $this->redisClient->expects($this->once())
            ->method('set');

        /**
         * Act.
         */
        $appCacheService = new AppCacheService($this->translator, $this->redisClient);
        $result = $appCacheService->set($testCacheKey, $testCacheValue, $testCacheTtl);

        /*
         * Assert
         */
        $this->assertTrue($result);
    }

    public function testShould_ReturnSavedValue_When_DataExistInCache(): void
    {
        /**
         * Arrange.
         */
        $testCacheKey = 'test_key';
        $testCacheValue = 'test_value';

        $this->redisClient->method('exists')
            ->willReturn(true);
        $this->redisClient->method('get')
            ->willReturn($testCacheValue);
        $resultValue = new class() {
            public function getPayload()
            {
                return 'OK';
            }
        };
        $this->redisClient->method('set')
            ->willReturn($resultValue);

        $this->redisClient->expects($this->once())
            ->method('set');
        $this->redisClient->expects($this->once())
            ->method('exists');
        $this->redisClient->expects($this->once())
            ->method('get');

        /**
         * Act.
         */
        $appCacheService = new AppCacheService($this->translator, $this->redisClient);
        $result = $appCacheService->set($testCacheKey, $testCacheValue);

        /*
         * Assert
         */
        $this->assertTrue($result);
        $this->assertEquals($testCacheValue, $appCacheService->get($testCacheKey));
    }

    public function tearDown(): void
    {
        parent::tearDown();

        $this->translator = null;
    }
}
