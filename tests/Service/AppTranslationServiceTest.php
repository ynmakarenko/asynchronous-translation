<?php

// tests/Service/AppTranslationServiceTest.php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Exception\TranslateException;
use App\Service\AppCacheService;
use App\Service\AppTranslationService;
use Panda\Yandex\TranslateSDK\Cloud;
use PHPUnit\Framework\MockObject\Stub;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AppTranslationServiceTest.
 */
class AppTranslationServiceTest extends TestCase
{
    /**
     * @var Stub|TranslatorInterface
     */
    private $translator;

    /**
     * @var AppCacheService|Stub
     */
    private $appCacheService;

    /**
     * @var Cloud|Stub
     */
    private $cloud;

    public function setUp(): void
    {
        parent::setUp();

        $this->translator = $this->createStub(TranslatorInterface::class);
        $this->translator
            ->method('trans')
            ->willReturn('error');
        $this->appCacheService = $this->createStub(AppCacheService::class);
        $this->appCacheService
            ->method('get')
            ->willReturn('{"languages": [{"code": "az","name": "azərbaycan"},{"code": "ru","name": "Russia"},{"code": "am","name": "አማርኛ"},{"code": "en","name": "English"}]}');
        $this->cloud = $this->createStub(Cloud::class);
    }

    public function testShould_ThrowException_When_TranslateEmptyString(): void
    {
        /**
         * Arrange.
         */
        $destinationLanguage = 'en';

        /*
         * Assert
         */
        $this->expectException(TranslateException::class);

        /**
         * Act.
         */
        $appTranslationService = new AppTranslationService($this->appCacheService, $this->translator, $this->cloud);
        $appTranslationService->translate('', $destinationLanguage);
    }

    public function testShould_ThrowException_When_DestinationLanguageNotExist(): void
    {
        /**
         * Arrange.
         */
        $testToTranslate = 'Hello';
        $destinationLanguage = 'not_existed_dest_lang';

        /*
         * Assert
         */
        $this->expectException(TranslateException::class);

        /**
         * Act.
         */
        $appTranslationService = new AppTranslationService($this->appCacheService, $this->translator, $this->cloud);
        $appTranslationService->translate($testToTranslate, $destinationLanguage);
    }

    public function testShould_ReturnObject_When_TranslationIsCompleted(): void
    {
        /**
         * Arrange.
         */
        $testToTranslate = 'Hello';
        $destinationLanguage = 'ru';

        $this->cloud->method('request')
            ->willReturn('{"translations": [{"text": "Привет","detectedLanguageCode": "en"}]}');

        /**
         * Act.
         */
        $appTranslationService = new AppTranslationService($this->appCacheService, $this->translator, $this->cloud);
        $result = $appTranslationService->translate($testToTranslate, $destinationLanguage);

        /*
         * Assert
         */
        $this->assertIsObject($result);
        $this->assertObjectHasAttribute('translations', $result);
        $this->assertIsArray($result->translations);
        $this->assertCount(1, $result->translations);
        $this->assertIsObject($result->translations[0]);
        $this->assertObjectHasAttribute('text', $result->translations[0]);
        $this->assertObjectHasAttribute('detectedLanguageCode', $result->translations[0]);
        $this->assertEquals('Привет', $result->translations[0]->text);
        $this->assertEquals('en', $result->translations[0]->detectedLanguageCode);
    }

    public function testShould_ReturnLanguagesList(): void
    {
        /**
         * Act.
         */
        $appTranslationService = new AppTranslationService($this->appCacheService, $this->translator, $this->cloud);
        $langList = $appTranslationService->getLanguagesList();

        /*
         * Assert
         */
        $this->assertIsArray($langList);
        $this->assertArrayHasKey('English', $langList);
        $this->assertEquals('en', $langList['English']);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->translator = null;
        $this->appCacheService = null;
    }
}
